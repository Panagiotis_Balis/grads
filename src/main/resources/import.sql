



--Used to instantiate the bundle instances in the Database, will run every time until spring.jpa.hibernate.ddl-auto property is set to 'update'
insert into PRODUCTS (id, name, price, product_code, expiration_date, availability_date) values (1,'SERVICE1',5.22, 'CODEAB',{ts '2012-09-17 18:47:52.69'}, {ts '2012-09-17 18:47:52.69'});
insert into PRODUCTS (id, name, price, product_code, availability_date) values (2,'SERVICE2',7.00, 'CODEBC', {ts '2020-09-17 18:47:52.69'});
insert into PRODUCTS (id, name, price, product_code, expiration_date, availability_date) values (3,'SERVICE3',1.22, 'CODEWWW',{ts '2012-09-17 18:47:52.69'}, {ts '2023-09-1 18:47:52.69'});
insert into PRODUCTS (id, name, price, product_code, expiration_date, availability_date) values (4,'SERVICE4',14.68, 'CODGRE',{ts '2012-09-17 18:47:52.69'}, {ts '2012-09-4 18:47:52.69'});
insert into PRODUCTS (id, name, price, product_code, expiration_date, availability_date) values (5,'SERVICE5',9.32, 'CODECDE',{ts '2012-09-17 18:47:52.69'}, {ts '2045-09-16 18:47:52.69'});
insert into PRODUCTS (id, name, price, product_code, expiration_date, availability_date) values (6,'SERVICE6',9.22, 'XXXXX',{ts '2012-09-17 18:47:52.69'}, {ts '2066-09-17 18:47:52.69'});
insert into PRODUCTS (id, name, price, product_code, expiration_date, availability_date) values (7,'SERVICE7',14.65, 'CORTRE',{ts '2012-09-17 18:47:52.69'}, {ts '2044-09-27 18:47:52.69'});
insert into PRODUCTS (id, name, price, product_code, expiration_date, availability_date) values (8,'SERVICE8',8.92, 'ERDFVE',{ts '2012-09-17 18:47:52.69'}, {ts '2020-09-4 18:47:52.69'});
insert into PRODUCTS (id, name, price, product_code, expiration_date, availability_date) values (9,'SERVICE9',18.55, 'XXX777',{ts '2012-09-17 18:47:52.69'}, {ts '2020-09-17 18:47:52.69'});

