package com.balisPanagiotis.gradProject.service;

import com.balisPanagiotis.gradProject.domain.Product;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductService {

    Product findProductById(Long id);

    List<Product> findAllProducts(String sortBy, String order);

    Product saveProduct(Product product);

    Product deleteProduct(Product product);

    List<Product> getProductsByNameFragment(String nameFragment);

    List<Product> getProductsByProductCodeFragment(String codeFragment);

    List<Product> getProductsByPrice(Double minValue, Double maxValue, String order);
}
