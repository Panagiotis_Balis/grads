package com.balisPanagiotis.gradProject.service;

import com.balisPanagiotis.gradProject.domain.Product;
import com.balisPanagiotis.gradProject.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product findProductById(Long id) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.get() == null) {
            return null;
        } else
            return optionalProduct.get();
    }

    @Override
    public List<Product> findAllProducts(String sortBy, String order) {

        sortBy = ((sortBy == null) ? "id" : sortBy);
        order = ((order == null) ? "asc" : order);
        if (order.equals("desc")) {
            return productRepository.findAll(Sort.by(sortBy).descending());
        } else {
            return productRepository.findAll(Sort.by(sortBy).ascending());
        }
    }

    @Override
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product deleteProduct(Product product) {
        productRepository.delete(product);
        return product;
    }

    @Override
    public List<Product> getProductsByNameFragment(String nameFragment) {
        if (nameFragment == null) nameFragment = "";
        return productRepository.findByNameContaining(nameFragment);
    }

    @Override
    public List<Product> getProductsByProductCodeFragment(String codeFragment) {
        if (codeFragment == null) codeFragment = "";
        return productRepository.findByProductCodeContaining(codeFragment);
    }

    private static final Double MINVALUE = 0.00;
    private static final Double MAXVALUE = 1000000.00;

    @Override
    public List<Product> getProductsByPrice(Double minValue, Double maxValue, String order) {
        if (minValue == null) minValue = MINVALUE;
        if (maxValue == null) maxValue = MAXVALUE;
        if (order == null) order = " ";

        if (order.equals("desc")) {
            return productRepository.findByPriceBetweenOrderByPriceDesc(minValue, maxValue);
        }
        return productRepository.findByPriceBetweenOrderByPriceAsc(minValue, maxValue);
    }


}
