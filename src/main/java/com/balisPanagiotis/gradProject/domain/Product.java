package com.balisPanagiotis.gradProject.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "PRODUCTS", uniqueConstraints = {@UniqueConstraint(columnNames = {"NAME"})})

public class Product {

    private static final int MAX_NAME_LENGTH = 100;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "NAME", length = MAX_NAME_LENGTH, nullable = false)
    private String name;

    @Column(name = "PRICE", nullable = false)
    private Double price;

    @Column(name = "PRODUCT_CODE", nullable = false)
    private String productCode;

    @Column(name = "EXPIRATION_DATE")
    private LocalDate expirationDate;

    @Column(name = "AVAILABILITY_DATE", nullable = false)
    private LocalDate availabilityDate;


}
