package com.balisPanagiotis.gradProject.controller;

import com.balisPanagiotis.gradProject.domain.Product;
import com.balisPanagiotis.gradProject.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products/{id}")
    ResponseEntity<Product> getProduct(@PathVariable(required = false) Long id) {

        Product product = productService.findProductById(id);
        return ResponseEntity.status(HttpStatus.OK).body(product);
    }

    @GetMapping("/products")
    ResponseEntity<List<Product>> getAllProducts(@RequestParam(value = "sortBy", required = false) String sortBy,
                                                 @RequestParam(value = "order", required = false) String order) {

        List<Product> myList = productService.findAllProducts(sortBy, order);
        return ResponseEntity.status(HttpStatus.OK).body(myList);
    }

    @PostMapping(path = "/products/add", consumes = "application/json", produces = "application/json")
    ResponseEntity<Product> addProduct(@RequestBody Product product) {
        Product savedProduct = productService.saveProduct(product);
        return ResponseEntity.status(HttpStatus.OK).body(savedProduct);
    }

    @PostMapping(path = "/products/delete", consumes = "application/json", produces = "application/json")
    ResponseEntity<Product> deleteProduct(@RequestBody Product product) {
        Product deletedProduct = productService.deleteProduct(product);
        return ResponseEntity.status(HttpStatus.OK).body(deletedProduct);
    }

    @GetMapping("products/queryByName")
    ResponseEntity<List<Product>> getproductsByName(@RequestParam(value = "nameFragment", required = false) String nameFragment) {

        List<Product> myList = productService.getProductsByNameFragment(nameFragment);
        return ResponseEntity.status(HttpStatus.OK).body(myList);
    }

    @GetMapping("products/queryByCode")
    ResponseEntity<List<Product>> getproductsByProductCode(@RequestParam(value = "codeFragment", required = false) String codeFragment) {

        List<Product> myList = productService.getProductsByProductCodeFragment(codeFragment);
        return ResponseEntity.status(HttpStatus.OK).body(myList);
    }


    @GetMapping("products/queryByPrice")
    ResponseEntity<List<Product>> getproductsByPrice(@RequestParam(value = "minValue", required = false) Double minValue,
                                                     @RequestParam(value = "maxValue", required = false) Double maxValue,
                                                     @RequestParam(value = "order", required = false) String order) {

        List<Product> myList = productService.getProductsByPrice(minValue, maxValue, order);
        return ResponseEntity.status(HttpStatus.OK).body(myList);
    }


}
