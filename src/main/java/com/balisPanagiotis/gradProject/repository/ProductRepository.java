package com.balisPanagiotis.gradProject.repository;

import com.balisPanagiotis.gradProject.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {


    List<Product> findByNameContaining(String nameFragment);

    List<Product> findByProductCodeContaining(String codeFragment);

    List<Product> findByPriceBetweenOrderByPriceAsc(Double minValue, Double maxValue);

    List<Product> findByPriceBetweenOrderByPriceDesc(Double minValue, Double maxValue);

}
